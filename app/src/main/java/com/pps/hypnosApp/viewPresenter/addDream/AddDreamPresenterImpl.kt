package com.pps.hypnosApp.viewPresenter.addDream

import Const.Category.DEFAULT_NAME
import Const.Dream.DEFAULT_ID
import com.pps.hypnosApp.networking.DreamApi
import com.pps.hypnosApp.networking.RestApiManager
import com.pps.hypnosApp.viewPresenter.base.BasePresenterImpl
import Dream
import Category
import Const.AlternativesCategories.LONG_DREAM
import Const.AlternativesCategories.SHORT_DREAM
import Const.Length.LENGTH_THRESHOLD
import DreamAddingPayload
import com.pps.hypnosApp.R
import com.pps.hypnosApp.utils.Prefs
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.*

/**
 * The presenter used to add a dream
 */
class AddDreamPresenterImpl : BasePresenterImpl<AddDreamContract.AddDreamView>(), AddDreamContract.AddDreamPresenter{

    override fun onDoneButtonClicked(dreamText: String) {

        fun lengthChooser(){
            with(Prefs) {
                if(StringTokenizer(dreamText).countTokens() > LENGTH_THRESHOLD) {
                    dreamAddPositiveIds.add(LONG_DREAM.toString())
                    dreamAddPositiveIds.remove(SHORT_DREAM.toString())
                } else {
                    dreamAddPositiveIds.remove(LONG_DREAM.toString())
                    dreamAddPositiveIds.add(SHORT_DREAM.toString())
                }
            }

        }

        lengthChooser()
        val categoryList = Prefs.dreamAddPositiveIds.map {Category(id = it.toInt(), name = DEFAULT_NAME) }
        RestApiManager.createService(DreamApi::class.java)
                .addDream(DreamAddingPayload(
                            Dream(DEFAULT_ID, dreamText), categoryList))
                .onErrorReturn { this }
                .doOnSubscribe{view?.startLoadingState()}
                .doAfterTerminate{view?.stopLoadingState()}
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError { view?.showError(R.string.connection_error) }
                .subscribe(
                        {
                            view?.resumeMainView()
                        }
                )
    }



}
