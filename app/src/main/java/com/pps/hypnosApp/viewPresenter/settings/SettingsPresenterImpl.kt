package com.pps.hypnosApp.viewPresenter.settings

import com.pps.hypnosApp.viewPresenter.base.BasePresenterImpl
import Category
import Const.AlternativesCategories.LONG_DREAM
import Const.AlternativesCategories.SHORT_DREAM
import com.pps.hypnosApp.R
import com.pps.hypnosApp.networking.CategoryApi
import com.pps.hypnosApp.networking.RestApiManager
import com.pps.hypnosApp.utils.Prefs
import com.pps.hypnosApp.utils.incrementAndToString
import io.reactivex.android.schedulers.AndroidSchedulers

class SettingsPresenterImpl:BasePresenterImpl<SettingsContract.SettingsView>(), SettingsContract.SettingsPresenter{

    override var categoryList: MutableList<Category> = mutableListOf()

    override fun onActivityStarted() {

        RestApiManager.createService(CategoryApi::class.java)
                .getAllCategories()
                .onErrorReturn { listOf() }
                .doOnSubscribe{ view?.startLoadingState()}
                .doAfterTerminate { view?.stopLoadingState() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { resultList ->
                            when(resultList.isEmpty()){
                            true -> view?.showError(R.string.connection_error)
                            else -> with(this@SettingsPresenterImpl.categoryList) {
                                    clear()
                                    addAll(resultList)
                                    if(Prefs.isAdding){
                                        categoryList = categoryList.filter { it.id != SHORT_DREAM && it.id != LONG_DREAM }.toMutableList()
                                    }
                                    view?.showCategories()
                                }
                            }
                        }
                )
    }

    override fun onCategoryClicked(categoryIndex: Int, isSelected: Boolean) {
        with(Prefs) {

            if (isSelected && isAdding) {
                dreamAddPositiveIds.add(categoryIndex.incrementAndToString())
            } else if (isSelected && !isAdding) {
                dreamSelectionNegativeIds.remove(categoryIndex.incrementAndToString())
            } else if (!isSelected && isAdding) {
                dreamAddPositiveIds.remove(categoryIndex.incrementAndToString())
            } else {
                dreamSelectionNegativeIds.add(categoryIndex.incrementAndToString())
            }
        }
    }

    override fun onDoneButtonClicked(){
        if(!Prefs.isAdding) {
            with(Prefs.dreamSelectionNegativeIds) {
                when (!contains(SHORT_DREAM.toString()) || !contains(LONG_DREAM.toString())) {
                    true -> view?.startDreamActivity()
                    else -> view?.showSettingsError()
                }
            }
        }else{
            view?.resumeAddDreamActivity()
        }
    }


}

