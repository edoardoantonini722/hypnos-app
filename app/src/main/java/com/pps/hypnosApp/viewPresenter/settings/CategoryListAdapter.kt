package com.pps.hypnosApp.viewPresenter.settings

import android.support.v7.widget.RecyclerView
import android.view.View
import Category
import android.view.ViewGroup
import com.pps.hypnosApp.R
import com.pps.hypnosApp.utils.Prefs
import com.pps.hypnosApp.utils.inflate
import com.pps.hypnosApp.utils.onInnerCheckBoxStatusChange
import kotlinx.android.synthetic.main.item_category.view.*

/**
 * The adapter used for the category recycler view
 * @param [categoryList] the list of the categories to show
 * @param [onClickListener] the lambda to call when the user clicks on an item
 */
class CategoryListAdapter(private val categoryList: List<Category>,
                          private val onClickListener: (View, Int, Boolean, Int) -> Unit) : RecyclerView.Adapter<CategoryListAdapter.CategoryViewHolder>(){


    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.bind(categoryList[position])
    }

    override fun getItemCount(): Int = categoryList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : CategoryViewHolder {
        val holder = CategoryViewHolder(parent.inflate(R.layout.item_category))
        holder.onInnerCheckBoxStatusChange(event = onClickListener)
        return holder
    }

    class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        fun bind(itemCategory: Category){
            with(itemView.categoryCheckBox) {
                isChecked = false
                text = itemCategory.name
                when(Prefs.isAdding){
                    true -> if (Prefs.dreamAddPositiveIds.contains(itemCategory.id.toString())) isChecked = true
                    else -> if (!Prefs.dreamSelectionNegativeIds.contains(itemCategory.id.toString())) isChecked = true
                }
            }
        }
    }

}
