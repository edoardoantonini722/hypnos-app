package com.pps.hypnosApp.viewPresenter.settings

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import com.pps.hypnosApp.viewPresenter.base.BaseActivity
import android.os.Bundle
import android.view.View
import com.chibatching.kotpref.Kotpref
import com.pps.hypnosApp.R
import kotlinx.android.synthetic.main.settings_layout.*

/**
 * The activity used to select the settings
 */
class SettingsActivity : BaseActivity<SettingsContract.SettingsView,SettingsContract.SettingsPresenter>(), SettingsContract.SettingsView {

    override val layout = R.layout.settings_layout
    override var presenter: SettingsContract.SettingsPresenter = SettingsPresenterImpl()

    private lateinit var itemOnClick: (View, Int, Boolean, Int) -> Unit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Kotpref.init(this)

        btnCategorySelectionDone.setOnClickListener { presenter.onDoneButtonClicked() }

        itemOnClick = { _, position, isChecked, _ -> presenter.onCategoryClicked(position, isChecked) }

        presenter.onActivityStarted()
    }

    override fun showCategories() {
        runOnUiThread {
            with(rvCategoryList) {
                adapter = null
                layoutManager = null
                layoutManager = android.support.v7.widget.LinearLayoutManager(this@SettingsActivity, android.widget.LinearLayout.VERTICAL, false)
                adapter = CategoryListAdapter(presenter.categoryList.sortedBy { it.id }, itemOnClick)
                adapter.notifyDataSetChanged()
            }
        }
    }

    override fun startDreamActivity() {
        val returnIntent = Intent()
        setResult(Activity.RESULT_OK,returnIntent)
        finish()
    }


    override fun stopLoadingState() {
        runOnUiThread {
            categorySpinner.visibility = View.GONE
        }
    }

    override fun startLoadingState() {
        runOnUiThread {
            categorySpinner.visibility = View.VISIBLE
        }
    }

    override fun showSettingsError() {
        AlertDialog.Builder(this@SettingsActivity)
                .setTitle(R.string.category_error_title)
                .setMessage(R.string.category_error_message)
                .setPositiveButton(R.string.ok_alert_dialog, { dialog, _ -> dialog.dismiss() })
                .create()
                .show()
    }

    override fun resumeAddDreamActivity() {
        finish()
    }



}
