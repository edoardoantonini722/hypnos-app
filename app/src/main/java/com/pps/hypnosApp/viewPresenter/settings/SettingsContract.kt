package com.pps.hypnosApp.viewPresenter.settings

import com.pps.hypnosApp.viewPresenter.base.BasePresenter
import com.pps.hypnosApp.viewPresenter.base.BaseView
import Category

/**
 * The contract for the settings logic
 */
interface SettingsContract{
    /**
     * An interface representing the functions for the view that shows the settings
     */
    interface SettingsView: BaseView{

        /**
         * Shows the categories
         */
        fun showCategories()

        /**
         * Starts the ShowDreamActivity
         */
        fun startDreamActivity()

        /**
         * Shows the settings error
         */
        fun showSettingsError()

        /**
         * Resumes the AddDreamActivity
         */
        fun resumeAddDreamActivity()
    }

    /**
     * An interface representing the functions for the presenter that handles the settings
     */
    interface SettingsPresenter: BasePresenter<SettingsView>{

        val categoryList: MutableList<Category>

        /**
         * Function called when the activity is stared
         */
        fun onActivityStarted()

        /**
         * Function called when a specified category is clicked
         * @param [categoryIndex] the index of the category
         * @param [isSelected] the boolean representing if the category is selected or not
         */
        fun onCategoryClicked(categoryIndex: Int,isSelected :Boolean)

        /**
         * Function called when done button is clicked
         */
        fun onDoneButtonClicked()

    }
}
