package com.pps.hypnosApp.viewPresenter.addDream

import com.pps.hypnosApp.viewPresenter.base.BasePresenter
import com.pps.hypnosApp.viewPresenter.base.BaseView

/**
 * The contract for the add dream logic
 */
interface AddDreamContract{

    /**
     * An interface representing the functions for the view that adds a dream
     */
    interface AddDreamView :BaseView{

        /**
         * Resumes the main view
         */
        fun resumeMainView()

        /**
         * Shows an alert if the user hasn't selected the categories
         */
        fun showCategoryAlert()
    }

    /**
     * An interface representing the function for the presenter that adds a dream
     */
    interface AddDreamPresenter : BasePresenter<AddDreamView>{

        /**
         * Callback used when done button is clicked
         * @param [dreamText] the text of the dream to add
         */
        fun onDoneButtonClicked(dreamText: String)
    }
}
