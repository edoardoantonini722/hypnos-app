package com.pps.hypnosApp.viewPresenter.base

/**
 * An interface containing the basic functions for a presenter
 * @param V the view
 */
interface BasePresenter<V : BaseView> {

    /**
     * Attaches the view
     * @param [view] the view
     */
    fun attachView(view: V)

    /**
     * Detaches the view
     */
    fun detachView()
}

/**
 * An abstract class representing a basic implementation of a Presenter
 */
abstract class BasePresenterImpl<V : BaseView> : BasePresenter<V> {

    protected var view: V? = null

    override fun attachView(view: V) {
        this.view = view
    }

    override fun detachView() {
        this.view = null
    }
}