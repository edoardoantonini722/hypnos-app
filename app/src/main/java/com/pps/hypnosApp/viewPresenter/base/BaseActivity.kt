package com.pps.hypnosApp.viewPresenter.base

import android.content.Context
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import org.jetbrains.anko.design.snackbar

/**
 * An interface with the base functions for a view
 */
interface BaseView {

    /**
     * Gets the context
     * @return the context
     */
    fun getContext(): Context

    /**
     * Shows an error
     * @param [error] the string representing the error
     */
    fun showError(error: String)

    /**
     * Shows an error
     * @param [errorResId] the string resource identifier representing the error
     */
    fun showError(@StringRes errorResId: Int)

    /**
     * Shows a message
     * @param [message] the string representing the message
     */
    fun showMessage(message: String)

    /**
     * Shows a message
     * @param [messageResId] the string resource identifier representing the message
     */
    fun showMessage(@StringRes messageResId: Int)

    /**
     * Stops the loading state (e.g. a spinner)
     */
    fun stopLoadingState(): Any


    /**
     * Starts the loading state (e.g. a spinner)
     */
    fun startLoadingState(): Any

}

/**
 * An abstract class representing a base activity
 * @param V the view type
 * @param P the presenter type
 */
@Suppress("UNCHECKED_CAST")
abstract class BaseActivity<V : BaseView, P : BasePresenter<V>> : AppCompatActivity(), BaseView {

    protected abstract var presenter: P
    protected abstract val layout: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        presenter.attachView(view = this as V)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

    override fun getContext(): Context = this@BaseActivity

    override fun showError(error: String) {
        snackbar(findViewById(android.R.id.content), error)
    }

    override fun showError(errorResId: Int) {
        snackbar(findViewById(android.R.id.content), errorResId)
    }

    override fun showMessage(message: String) {
        snackbar(findViewById(android.R.id.content), message)
    }

    override fun showMessage(messageResId: Int) {
        snackbar(findViewById(android.R.id.content), messageResId)
    }

}