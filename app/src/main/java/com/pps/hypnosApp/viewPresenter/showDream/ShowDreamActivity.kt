package com.pps.hypnosApp.viewPresenter.showDream

import Const.IntentUtils.SETTINGS_ACTIVITY_INTENT_NUMBER
import com.pps.hypnosApp.viewPresenter.base.BaseActivity
import Dream
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.chibatching.kotpref.Kotpref
import com.pps.hypnosApp.R
import com.pps.hypnosApp.utils.Prefs
import com.pps.hypnosApp.viewPresenter.addDream.AddDreamActivity
import com.pps.hypnosApp.viewPresenter.settings.SettingsActivity
import kotlinx.android.synthetic.main.dream_layout.*

/**
 * The activity used to show the dreams
 */
class ShowDreamActivity : BaseActivity<ShowDreamContract.ShowDreamView, ShowDreamContract.ShowDreamPresenter>(), ShowDreamContract.ShowDreamView {

    override var presenter: ShowDreamContract.ShowDreamPresenter = ShowDreamPresenterImpl()
    override val layout: Int = R.layout.dream_layout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Kotpref.init(this)
        setContentView(layout)
    }

    override fun onResume() {
        super.onResume()
        Prefs.isAdding = false
        Prefs.hasChoseCategory = false
        presenter.onNewDreamRequested()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
            val inflater = menuInflater
            inflater.inflate(R.menu.custom_dream_menu, menu)
            return true
    }

    override fun showDream(dream: Dream) {
        dreamArea.text = dream.text
    }

    override fun stopLoadingState() {
        runOnUiThread {
            loadingSpinner.visibility = View.GONE
        }
    }

    override fun startLoadingState() {
        runOnUiThread {
            loadingSpinner.visibility = View.VISIBLE
            dreamArea.text = ""
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when (item?.itemId) {
        R.id.action_settings -> {
            val intent = Intent(this,SettingsActivity::class.java)
            startActivityForResult(intent,SETTINGS_ACTIVITY_INTENT_NUMBER)
            true
        }

        R.id.action_sync -> {
            presenter.onNewDreamRequested()
            true
        }

        R.id.action_plus -> {
            val intent = Intent(this, AddDreamActivity::class.java)
            startActivity(intent)
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }
}
