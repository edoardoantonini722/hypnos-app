package com.pps.hypnosApp.viewPresenter.showDream

import com.pps.hypnosApp.viewPresenter.base.BaseView
import Dream
import com.pps.hypnosApp.viewPresenter.base.BasePresenter

/**
 * The contract for the dream showing logic
 */
interface ShowDreamContract {
    /**
     * An interface representing the functions for the view that shows the dreams
     */
    interface ShowDreamView : BaseView{

        /**
         * Shows a dream
         * @param [dream] the dream
         */
        fun showDream(dream: Dream)
    }

    /**
     * An interface representing the functions for the presenter that handles the dreams
     */
    interface ShowDreamPresenter : BasePresenter<ShowDreamView>{

        /**
         * Function called when a new dream is requested
         */
        fun onNewDreamRequested()
    }
}
