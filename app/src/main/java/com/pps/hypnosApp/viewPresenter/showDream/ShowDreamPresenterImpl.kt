package com.pps.hypnosApp.viewPresenter.showDream

import com.pps.hypnosApp.R
import com.pps.hypnosApp.networking.DreamApi
import com.pps.hypnosApp.networking.RestApiManager
import com.pps.hypnosApp.utils.Prefs
import com.pps.hypnosApp.utils.removeSpacesAndArrayDelimiters
import com.pps.hypnosApp.viewPresenter.base.BasePresenterImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.*

/**
 * The presenter used to show the dreams
 */
class ShowDreamPresenterImpl : BasePresenterImpl<ShowDreamContract.ShowDreamView>(), ShowDreamContract.ShowDreamPresenter {

    override fun onNewDreamRequested() {
        RestApiManager.createService(DreamApi::class.java)
                .getDreamsByCategories(
                        Arrays.toString(Prefs.dreamSelectionNegativeIds.toTypedArray()).removeSpacesAndArrayDelimiters())
                .onErrorReturn { listOf() }
                .doOnSubscribe{view?.startLoadingState()}
                .doAfterTerminate{view?.stopLoadingState()}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                            if(it.isEmpty()) view?.showError(R.string.connection_error)
                            else view?.showDream(it[Random().nextInt(it.size)])
                        }
                )
    }

}
