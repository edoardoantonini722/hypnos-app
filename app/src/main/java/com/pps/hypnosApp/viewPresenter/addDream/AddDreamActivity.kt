package com.pps.hypnosApp.viewPresenter.addDream

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.chibatching.kotpref.Kotpref
import com.pps.hypnosApp.R
import com.pps.hypnosApp.utils.Prefs
import com.pps.hypnosApp.viewPresenter.base.BaseActivity
import com.pps.hypnosApp.viewPresenter.settings.SettingsActivity
import kotlinx.android.synthetic.main.add_dream_layout.*

/**
 * The Activity used for adding a dream
 */
class AddDreamActivity : BaseActivity<AddDreamContract.AddDreamView,AddDreamContract.AddDreamPresenter>(), AddDreamContract.AddDreamView{

    override val layout = R.layout.add_dream_layout
    override var presenter : AddDreamContract.AddDreamPresenter = AddDreamPresenterImpl()
    private lateinit var imm: InputMethodManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Kotpref.init(this)
        Prefs.isAdding = true
        imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        addDreamArea.setOnClickListener {imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0)  }

    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.custom_add_dream_menu, menu)
        return true
    }

    override fun onPause() {
        super.onPause()
        imm.hideSoftInputFromWindow(addDreamArea.windowToken,0)
    }

    override fun onResume() {
        super.onResume()
        Prefs.isAdding = true
    }


    override fun stopLoadingState(){
        runOnUiThread {
            addDreamSpinner.visibility = View.GONE
        }
    }

    override fun startLoadingState() {
        runOnUiThread {
            addDreamSpinner.visibility = View.VISIBLE
        }
    }

    override fun resumeMainView() {
        Prefs.isAdding = false
        Prefs.hasChoseCategory = false
        finish()
    }

    override fun showCategoryAlert() {
        AlertDialog.Builder(this)
                .setTitle(R.string.want_to_continue)
                .setMessage(R.string.category_not_selected_message)
                .setPositiveButton(R.string.yes_alert_dialog, { _ , _ -> startSettingsActivity()})
                .setNegativeButton(R.string.no_alert_dialog, { _ , _ -> presenter.onDoneButtonClicked(addDreamArea.text.toString())})
                .create()
                .show()
    }

    private fun startSettingsActivity(){
        Prefs.hasChoseCategory = true
        val intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when (item?.itemId) {
        R.id.action_add_dream_settings -> {
            startSettingsActivity()
            true
        }

        R.id.action_add_dream_confirm -> {
            if(!Prefs.hasChoseCategory)
                showCategoryAlert()
            else
                presenter.onDoneButtonClicked(addDreamArea.text.toString())
            true
        }

        else -> {
            super.onOptionsItemSelected(item)
        }
    }



}
