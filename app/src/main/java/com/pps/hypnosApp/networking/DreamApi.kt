package com.pps.hypnosApp.networking

import Const.Dream.API_NAME
import Dream
import DreamAddingPayload
import Const.Dream.NEGATIVEIDS
import io.reactivex.Observable
import retrofit2.http.*
/**
 * The interface containing the API used for the Dream entity
 */
interface DreamApi{

    /**
     * Gets all the dreams that aren't in the specified categories
     * @param [negativeIds] the ids of the specified categories
     * @return the list of dreams matching the predicate
     */
    @GET("$API_NAME/categoryParams")
    fun getDreamsByCategories(@Query(NEGATIVEIDS) negativeIds: String) : Observable<List<Dream>>

    /**
     * Adds a dream to the database
     * @param dreamPayload the dream's data
     */
    @POST(API_NAME)
    fun addDream(@Body dreamPayload: DreamAddingPayload) : Observable<Any>
}
