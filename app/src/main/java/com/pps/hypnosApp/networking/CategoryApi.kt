package com.pps.hypnosApp.networking

import Const.Category.API_NAME
import io.reactivex.Observable
import retrofit2.http.GET
import Category

/**
 * The interface containing the API used for the Category entity
 */
interface CategoryApi{

    /**
     * Gets all the categories
     * @return the list of categories
     */
    @GET(API_NAME)
    fun getAllCategories() : Observable<List<Category>>
}
