package com.pps.hypnosApp.utils

import com.chibatching.kotpref.KotprefModel

/**
 * An object containing the handler for the Shared Preferences
 */
object Prefs: KotprefModel(){

    val dreamSelectionNegativeIds by stringSetPref{ return@stringSetPref HashSet<String>() }

    var isAdding by booleanPref(false)

    val dreamAddPositiveIds by stringSetPref{ return@stringSetPref HashSet<String>() }

    var ip by stringPref("2.234.121.101")

    var hasChoseCategory by booleanPref(false)
}