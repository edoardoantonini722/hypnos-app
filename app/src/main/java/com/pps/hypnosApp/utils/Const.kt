/**
 * A class containing useful constants
 */
object Const{

    object IntentUtils {
        const val SETTINGS_ACTIVITY_INTENT_NUMBER = 52
    }

    object AlternativesCategories {
        const val SHORT_DREAM = 5
        const val LONG_DREAM = 4
    }

    object Length {
        const val LENGTH_THRESHOLD = 200
    }


    object Dream {
        const val DEFAULT_ID = -52
        const val API_NAME = "dreams"
        const val NEGATIVEIDS = "negativeids"
    }

    object Category {
        const val DEFAULT_NAME = "default"
        const val API_NAME="categories"
    }

}