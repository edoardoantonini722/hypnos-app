package com.pps.hypnosApp.utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pps.hypnosApp.viewPresenter.settings.CategoryListAdapter
import kotlinx.android.synthetic.main.item_category.view.*

/**
 * A class containing extension functions
 */

/**
 * Inflates the resource in the view group
 * @param [layoutRes] the id of the resources
 */
fun ViewGroup.inflate(layoutRes: Int): View =
        LayoutInflater.from(context).inflate(layoutRes, this, false)

/**
 * Sets the call back for a checkbox inside an adapter
 * @param flatPosition the adapter's view holder position
 * @param event the call back to call
 * @return the adapter's view holder
 */
fun <T : CategoryListAdapter.CategoryViewHolder> T.onInnerCheckBoxStatusChange(flatPosition: Int? = null, event: (view: View, position: Int,isClicked: Boolean, type: Int) -> Unit): T {
    this.itemView.categoryCheckBox.setOnCheckedChangeListener { _ , isChecked ->
        event.invoke(this.itemView, flatPosition ?: this@onInnerCheckBoxStatusChange.adapterPosition, isChecked, this@onInnerCheckBoxStatusChange.itemViewType)
    }
    return this
}

/**
 * Removes spaces and array delimiters (e.g. []) from a string
 * @return the string modified
 */
fun String.removeSpacesAndArrayDelimiters(): String{
    return this.replace(" ","").replace("]","").replace("[","")
}

/**
 * Increment an integer and put it as a string
 * @return the string
 */
fun Int.incrementAndToString() : String{
    return (this+1).toString()
}
