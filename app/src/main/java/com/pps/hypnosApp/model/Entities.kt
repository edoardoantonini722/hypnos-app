/**
 * A Class containing all the entities
 */

/**
 * The Dream data class
 * @param [id] the dream id
 * @param [text] the dream body
 */
data class Dream(val id: Int, val text: String)

/**
 * The Category data class
 * @param [id] the category id
 * @param [name] the category name
 */
data class Category(val id: Int, val name: String)

/**
 * The data class representing the payload used to add a dream
 * @param [dream] the dream
 * @param [categoryList] the dream's categories
 */
data class DreamAddingPayload(val dream: Dream, val categoryList: List<Category>)
